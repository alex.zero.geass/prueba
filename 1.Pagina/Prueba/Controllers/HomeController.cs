﻿using Microsoft.AspNetCore.Mvc;
using Prueba.Models;
using System.Diagnostics;

namespace Prueba.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        [HttpGet]
        public JsonResult Login(string user, string pass)
        {
            string respuesta;
            try
            {
                if (!string.IsNullOrEmpty(user) && !string.IsNullOrEmpty(pass))
                    respuesta = "Conectariamos con el servidor.";
                else
                    respuesta = "Usuario o Contraseña vacios";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Json(respuesta);
        }
    }
}
