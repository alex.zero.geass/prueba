SELECT
  m.mensaje,
  p.nombre AS nombre_proyecto,
  pr.nombre AS nombre_producto
FROM MENSAJE m
JOIN PROYECTO p ON m.codigo_proyecto = p.codigo
JOIN PRODUCTO pr ON m.codigo_producto = pr.codigo;
