SELECT
  p.nombre AS nombre_proyecto,
  CASE
    WHEN COUNT(DISTINCT pr.nombre) > 1 THEN 'TODOS'
    ELSE pr.nombre
  END AS nombre_producto,
  m.mensaje
FROM MENSAJE m
JOIN PROYECTO p ON m.codigo_proyecto = p.codigo
JOIN PRODUCTO pr ON m.codigo_producto = pr.codigo
GROUP BY p.nombre, m.mensaje
HAVING COUNT(DISTINCT pr.nombre) > 1 OR COUNT(pr.nombre) = 1;
