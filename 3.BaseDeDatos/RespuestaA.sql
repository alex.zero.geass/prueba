SELECT
  p.nombre AS nombre_proyecto,
  pr.nombre AS nombre_producto
FROM PROYECTO p
JOIN PRODUCTO pr ON p.codigo = pr.codigo_proyecto
WHERE p.codigo = 1;
