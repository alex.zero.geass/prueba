﻿using MySql.Data.MySqlClient;
using System.Data;

namespace DBLayer
{
    public class DBHelper
    {
        private readonly string con;

        public DBHelper(string con)
        {
            this.con = con;
        }

        public DataSet ExecuteSelectQuery(string query)
        {
            using MySqlConnection connection = new(con);
            connection.Open();

            using MySqlCommand command = new (query, connection);
            MySqlDataAdapter adapter = new (command);
            DataSet dataSet = new ();
            adapter.Fill(dataSet);

            return dataSet;
        }

        public int ExecuteNonQuery(string query)
        {
            using MySqlConnection connection = new (con);
            connection.Open();

            using MySqlCommand command = new (query, connection);
            return command.ExecuteNonQuery();
        }
    }
}
